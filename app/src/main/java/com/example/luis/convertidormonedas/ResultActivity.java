package com.example.luis.convertidormonedas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {
    TextView textViewRsultado;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        textViewRsultado = (TextView) findViewById(R.id.textViewResultado);

        //String data = getIntent().getStringExtra( "valor");
        // 1 DOLAR * 0.8370
        String data = getIntent().getExtras().getString("valor");

        Double euro = Double.parseDouble(data);
        Double dolar = euro * 1.1977;
        Log.e("dato recibido", data);

        textViewRsultado.setText(dolar.toString());
    }
}
